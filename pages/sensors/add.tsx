import { GetServerSidePropsContext, NextPage, Redirect } from 'next'
import { FunctionComponent, useEffect } from 'react'
import { useSetUser } from '../../components/Modules/Auth/hooks/useUserInfo.hook'
import HeadingComponent from '../../components/Common/Heading.component'
import {
  extractCookies,
  prepareAccessKeysToSendAsHeader,
  redirectToAuth,
} from '../../components/Helpers/Functions'
import { AccessCookieName } from '../../components/Helpers/Variables'
import MainLayout from '../../components/Layouts/MainLayout'

import dynamic from 'next/dynamic'
import BackendService from '../../components/services/backend.service'
import { AccessKeys, User } from '../../components/Types/UserTypes'

const SensorForm = dynamic(
  () => import('../../components/Modules/Sensors/SensorsForm.component'),
  { ssr: false },
)

const SensorFormContext = dynamic(
  () =>
    import('../../components/Modules/Sensors/Hooks/useSensorForm.hook').then(
      (res) => res.SensorFormContext,
    ) as Promise<FunctionComponent>,
)

const AddSensorPage: NextPage<{ user: User }> = ({ user }) => {
  const setUser = useSetUser()

  useEffect(() => {
    setUser(user)
  }, [user, setUser])

  return (
    <MainLayout title="Создание датчика">
      <HeadingComponent title="Добавить датчик" />
      <SensorFormContext>
        <SensorForm type="create" />
      </SensorFormContext>
    </MainLayout>
  )
}

export async function getServerSideProps(
  context: GetServerSidePropsContext,
): Promise<{ props: { user: User } } | { redirect: Redirect }> {
  const accessKeys = extractCookies<AccessKeys>(context, AccessCookieName)
  const backendService = new BackendService(accessKeys)

  const result = await backendService.checkAuthAndTryRefreshTokens()

  if (result && result[0]) {
    const [user, newCookie] = result

    // Pass a new cookie with the access keys to the client-side
    if (newCookie)
      context.res.setHeader(
        'Set-Cookie',
        prepareAccessKeysToSendAsHeader(newCookie),
      )

    return {
      props: { user },
    }
    // If we cant identify the user - redirect them to the authorization page
  } else return redirectToAuth()
}
export default AddSensorPage
