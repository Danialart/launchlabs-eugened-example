import {GetServerSidePropsContext, NextPage, Redirect} from 'next';
import {FunctionComponent, useEffect} from 'react';
import {useSetUser} from '../../../components/Modules/Auth/hooks/useUserInfo.hook';
import HeadingComponent from '../../../components/Common/Heading.component';
import {
  extractCookies,
  prepareAccessKeysToSendAsHeader,
  redirectToAuth,
  showNotFoundError,
} from '../../../components/Helpers/Functions';
import {AccessCookieName} from '../../../components/Helpers/Variables';
import MainLayout from '../../../components/Layouts/MainLayout';
import BackendService from '../../../components/services/backend.service';
import SensorsService from '../../../components/services/sensors.service';
import {Sensor} from '../../../components/Types/Sensor.types';
import {AccessKeys, User} from '../../../components/Types/UserTypes';
import dynamic from 'next/dynamic';

const SensorForm = dynamic(
  () => import('../../../components/Modules/Sensors/SensorsForm.component'),
  {ssr: false},
);

const SensorFormContext = dynamic(
  () =>
    import('../../../components/Modules/Sensors/Hooks/useSensorForm.hook').then(
      (res) => res.SensorFormContext,
    ) as Promise<FunctionComponent<{initialData?: Sensor | undefined}>>,
);

const EditSensorPage: NextPage<{user: User; sensor: Sensor}> = ({
  user,
  sensor,
}) => {
  const setUser = useSetUser();

  useEffect(() => {
    setUser(user);
  }, [user, setUser]);

  return (
    <MainLayout title={`Редактирование датчика ${sensor.number}`}>
      <HeadingComponent title={`Редактировать датчик ${sensor.number}`} />
      <SensorFormContext initialData={sensor}>
        <SensorForm type="edit" />
      </SensorFormContext>
    </MainLayout>
  );
};

export async function getServerSideProps(
  context: GetServerSidePropsContext,
): Promise<
  | {props: {user: User; sensor: Sensor}}
  | {redirect: Redirect}
  | {notFound: boolean}
> {
  const sensorId = context.query.id && +context.query.id;
  // Redirect to not found page if sensor ID is not found in URL
  if (!sensorId) return showNotFoundError();

  const accessKeys = extractCookies<AccessKeys>(context, AccessCookieName);

  const backendService = new BackendService(accessKeys);

  const result = await backendService.checkAuthAndTryRefreshTokens();

  if (result && result[0]) {
    const [user, newCookie] = result;

    // Pass a new cookie with the access keys to the client-side
    if (newCookie)
      context.res.setHeader(
        'Set-Cookie',
        prepareAccessKeysToSendAsHeader(newCookie),
      );

    const sensorService = new SensorsService(newCookie || accessKeys);

    // todo: implement request for a sensor information
    const sensor = await sensorService.getSensor(sensorId);

    if (sensor) {
      return {
        props: {
          user,
          sensor: sensor,
        },
      };
    } else return showNotFoundError();

    // If we cant identify the user - redirect them to the authorization page
  } else return redirectToAuth();
}
export default EditSensorPage;
