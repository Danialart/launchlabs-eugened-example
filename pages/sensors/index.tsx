import type { GetServerSidePropsContext, NextPage } from 'next'
import router from 'next/router'
import { useEffect } from 'react'
import { Button } from 'reactstrap'
import { useSetUser } from '../../components/Modules/Auth/hooks/useUserInfo.hook'
import HeadingComponent from '../../components/Common/Heading.component'
import {
  extractCookies,
  prepareAccessKeysToSendAsHeader,
  redirectToAuth,
} from '../../components/Helpers/Functions'
import { AccessCookieName } from '../../components/Helpers/Variables'
import MainLayout from '../../components/Layouts/MainLayout'
import BackendService from '../../components/services/backend.service'
import {
  AccessKeys,
  ServerSideUserProps,
  User,
} from '../../components/Types/UserTypes'
import SensorsTableWrapper from '../../components/Modules/Sensors/SensorsTableWrapper.compoent'
import { SensorTableContextWrapper } from '../../components/Modules/Sensors/Hooks/useSensorsTable'

const Home: NextPage<{ user: User }> = ({ user }) => {
  const setUser = useSetUser()

  useEffect(() => {
    // Store received User info from the server-side in the UserContext to get access to this information from any component
    setUser(user)
  }, [setUser, user])

  return (
    <MainLayout title="Список датчиков">
      <HeadingComponent title="Датчики">
        <Button
          className="ms-auto"
          onClick={() => router.push('/sensors/add')}
          color="primary"
        >
          Добавить датчик
        </Button>
      </HeadingComponent>
      <SensorTableContextWrapper>
        <SensorsTableWrapper />
      </SensorTableContextWrapper>
    </MainLayout>
  )
}

export async function getServerSideProps(
  context: GetServerSidePropsContext,
): Promise<ServerSideUserProps> {
  const accessKeys = extractCookies<AccessKeys>(context, AccessCookieName)

  const backendService = new BackendService(accessKeys)

  const result = await backendService.checkAuthAndTryRefreshTokens()

  if (result && result[0]) {
    const [user, newCookie] = result

    // Pass a new cookie with the access keys to the client-side
    if (newCookie)
      context.res.setHeader(
        'Set-Cookie',
        prepareAccessKeysToSendAsHeader(newCookie),
      )

    return {
      props: { user },
    }
    // If we cant identify the user - redirect them to the authorization page
  } else return redirectToAuth()
}

export default Home
