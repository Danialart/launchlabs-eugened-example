import { GetServerSidePropsContext, NextPage, Redirect } from "next";
import dynamic from "next/dynamic";
import { Col, Row, Table } from "reactstrap";
import HeadingComponent from "../../../components/Common/Heading.component";
import {
  showNotFoundError,
  extractCookies,
  prepareAccessKeysToSendAsHeader,
  redirectToAuth,
} from "../../../components/Helpers/Functions";
import { AccessCookieName } from "../../../components/Helpers/Variables";
import MainLayout from "../../../components/Layouts/MainLayout";
import { NoticeContextWrapper } from "../../../components/Modules/Notices/Hooks/useNoticesTable.hook";
import BackendService from "../../../components/services/backend.service";
import SensorsService from "../../../components/services/sensors.service";
import { Sensor } from "../../../components/Types/Sensor.types";
import { User, AccessKeys } from "../../../components/Types/UserTypes";

const SensorNoticesList = dynamic(
  () =>
    import("../../../components/Modules/Sensors/SensorNoticesList.component")
);

const ViewSensor: NextPage<{ sensor: Sensor }> = ({ sensor }) => {
  return (
    <MainLayout title={`Просмотр датчика ${sensor.number}`}>
      <HeadingComponent title={`Просмотр датчика ${sensor.number}`} />
      <Row>
        <Col md="5" lg="4">
          <Table>
            <tbody>
              <tr>
                <td>Название</td>
                <td>{sensor.title}</td>
              </tr>
              <tr>
                <td>Широта</td>
                <td>{sensor.lat}</td>
              </tr>
              <tr>
                <td>Долгота</td>
                <td>{sensor.lng}</td>
              </tr>
            </tbody>
          </Table>
        </Col>
      </Row>
      <hr />
      <NoticeContextWrapper sensor={sensor}>
        <SensorNoticesList />
      </NoticeContextWrapper>
    </MainLayout>
  );
};

export async function getServerSideProps(
  context: GetServerSidePropsContext
): Promise<
  | { props: { user: User; sensor: Sensor } }
  | { redirect: Redirect }
  | { notFound: boolean }
> {
  const sensorId = context.query.id && +context.query.id;
  // Redirect to not found page if sensor ID is not found in URL
  if (!sensorId) return showNotFoundError();

  const accessKeys = extractCookies<AccessKeys>(context, AccessCookieName);

  const backendService = new BackendService(accessKeys);

  const result = await backendService.checkAuthAndTryRefreshTokens();

  if (result && result[0]) {
    const [user, newCookie] = result;

    // Pass a new cookie with the access keys to the client-side response
    if (newCookie)
      context.res.setHeader(
        "Set-Cookie",
        prepareAccessKeysToSendAsHeader(newCookie)
      );

    const sensorService = new SensorsService(newCookie || accessKeys);

    // todo: implement request for a sensor information
    const sensor = await sensorService.getSensor(sensorId);

    if (sensor) {
      return {
        props: {
          user,
          sensor: sensor,
        },
      };
    } else return showNotFoundError();

    // If we cant identify the user - redirect them to the authorization page
  } else return redirectToAuth();
}
export default ViewSensor;
