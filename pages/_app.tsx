import 'bootstrap/scss/bootstrap.scss';
import 'bootstrap-icons/font/bootstrap-icons.css';
import 'react-toastify/dist/ReactToastify.min.css';
import 'nprogress/nprogress.css';
import '../styles/global.scss';
import type {AppProps} from 'next/app';
import {ToastContainer} from 'react-toastify';
import {UserContextProvider} from '../components/Modules/Auth/hooks/useUserInfo.hook';
import dynamic from 'next/dynamic';

const ProgressBar = dynamic(
  () => {
    return import('../components/Common/ProgressBar.component');
  },
  {ssr: false},
);

function MyApp({Component, pageProps}: AppProps) {
  return (
    <UserContextProvider>
      <ProgressBar />
      <Component {...pageProps} />
      <ToastContainer
        position="bottom-right"
        autoClose={7000}
        hideProgressBar={false}
        newestOnTop={true}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        theme="colored"
        pauseOnHover
      />
    </UserContextProvider>
  );
}

export default MyApp;
