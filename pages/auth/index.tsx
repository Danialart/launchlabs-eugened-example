import { NextPage } from "next";
import AuthForm from "../../components/Modules/Auth/AuthForm.component";
import NotAuthorizedLayout from "../../components/Layouts/NotAuthorizedLayout";

const AuthPage: NextPage = () => {
  return (
    <NotAuthorizedLayout>
      <AuthForm />
    </NotAuthorizedLayout>
  );
};

export default AuthPage;
