import {Redirect} from 'next';

export default function Index() {
  return <div></div>;
}

export async function getServerSideProps(): Promise<{redirect: Redirect}> {
  return {
    redirect: {
      destination: '/sensors',
      permanent: false,
    },
  };
}
