import { FilterSensors, Sensor } from "../Types/Sensor.types";

import BackendService from "./backend.service";

class SensorsService extends BackendService {
  getSensors(filter?: FilterSensors): Promise<[Sensor[], number]> {
    return this.request<[Sensor[], number]>("GET", "sensors/filter", filter);
  }

  getSensor(id: number) {
    return this.request<Sensor>("get", `sensors/sensor/${id}`);
  }

  deleteSensor(id: number) {
    return this.request<void>("DELETE", `sensors/sensor/${id}`);
  }

  createSensor(sensor: Sensor) {
    return this.request<Sensor>("put", "sensors/create", sensor);
  }

  toggleSensor(id: number) {
    return this.request<void>("patch", `sensors/sensor/${id}/switch`);
  }

  updateSensor(sensor: Sensor) {
    return this.request<Sensor>(
      "patch",
      `sensors/sensor/${sensor.id}/update`,
      sensor
    );
  }
}

export default SensorsService;
