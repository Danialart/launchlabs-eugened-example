import { FilterUsers, User } from "../Types/UserTypes";
import BackendService from "./backend.service";

type UsersListType = [User[], number];

export default class UserService extends BackendService {
  /**
   * Filter users list
   * @param filter filter by search query
   * @returns
   */
  getUsers(filter?: FilterUsers): Promise<UsersListType> {
    return this.request<UsersListType>("get", "users/all", filter);
  }

  /**
   * Delete user
   * @param id user id
   * @returns
   */
  deleteUser(id: number) {
    return this.request("DELETE", `users/user/${id}`);
  }

  /**
   * Register a new user
   * @param data User info
   * @returns promise with User
   */
  addUser(data: Omit<User, "id">) {
    return this.request<User>("put", "users/register", data);
  }

  /**
   * Update user by ID
   * @param id of user
   * @param data user data
   * @returns updated user information
   */
  updateUser(id: number, data: Omit<User, "id">) {
    return this.request<User>("post", `users/${id}/update`, data);
  }

  /**
   * Get one speficif user
   * @param id user id
   */
  async getUserById(id: number) {
    return this.request<User>("get", `users/${id}/get`);
  }
}
