import {FunctionComponent} from 'react';
import {Button, Form, FormGroup, Input, Label} from 'reactstrap';
import styles from '../../../styles/components/Auth/AuthForm.module.scss';
import useAuth from './hooks/useAuth.hook';
import classNames from 'classnames';

const AuthForm: FunctionComponent = () => {
  const {handleLogin, login, password, setLoginEmail, setLoginPassword} =
    useAuth();

  return (
    <>
      <h2 className="text-center my-4">Вход в систему</h2>
      <Form
        className={classNames(styles.AuthForm, 'my-5 mx-auto')}
        onSubmit={(e) => {
          e.preventDefault();
          handleLogin();
        }}
      >
        <FormGroup>
          <Label for="email">Ваш E-mail:</Label>
          <Input
            type="email"
            id="email"
            required
            placeholder="Введите e-mail"
            value={login}
            onChange={(e) => setLoginEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="password">Ваш пароль:</Label>
          <Input
            required
            type="password"
            id="password"
            value={password}
            onChange={(e) => setLoginPassword(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Button color="primary">Вход в систему</Button>
        </FormGroup>
      </Form>
    </>
  );
};

export default AuthForm;
