import { AxiosError } from "axios";
import { useCallback, useState } from "react";
import { toast } from "react-toastify";
import BackendService from "../../../services/backend.service";

import { AccessCookieName } from "../../../Helpers/Variables";
import { useRouter } from "next/router";
import { Role } from "../../../Types/UserTypes";
import useHandlingMessages from "../../../Common/hooks/useHandlingMessages.hook";

type LoginData = {
  email: string;
  password: string;
};

export default function useAuth() {
  const [loginData, setLoginData] = useState<LoginData>({
    email: "",
    password: "",
  });

  const { handleServicesError } = useHandlingMessages();

  const router = useRouter();

  const handleLogin = useCallback(() => {
    const backendService = new BackendService();

    backendService
      .login(loginData.email, loginData.password)
      .then((res) => res.data)
      .then((userData) => {
        // Check if user is admin
        if (!userData.user.role?.find((role) => role.role_name === Role.Admin))
          return toast.error("Войдите в аккаунт с правами администратора!");
        // Store access keys inside the Browser cookies (for accessing it from server-side)

        // Set cookie
        backendService.setCookies(userData[AccessCookieName]);
        // Redirect to main page
        router.push("/");
      })
      .catch((error: AxiosError) => {
        handleServicesError(error, "Ошибка логина или пароля!");
      });
  }, [loginData, router, handleServicesError]);

  const setLoginEmail = useCallback(
    (email: LoginData["email"]) =>
      setLoginData({
        ...loginData,
        email,
      }),
    [loginData]
  );

  const setLoginPassword = useCallback(
    (password: LoginData["password"]) =>
      setLoginData({
        ...loginData,
        password,
      }),
    [loginData]
  );

  return {
    loginData,
    login: loginData.email,
    password: loginData.password,
    setLoginPassword,
    setLoginEmail,
    handleLogin,
  };
}
