import {createContext, FunctionComponent, useContext, useState} from 'react';
import {AccessKeys, User} from '../../../Types/UserTypes';

type UserInfo = {
  setUser: (userInfo: User, accessKeys?: AccessKeys) => void;
  userinfo: User;
};

export const UserContext = createContext<UserInfo>({} as UserInfo);

function useUserInfo(): UserInfo {
  const [userinfo, setUserinfo] = useState<User>({} as User);

  // Set user info to the context
  const setUser = (userInfo: User) => {
    setUserinfo(userInfo);
  };

  return {setUser, userinfo};
}

export const UserContextProvider: FunctionComponent = ({children}) => {
  return (
    <UserContext.Provider value={useUserInfo()}>
      {children}
    </UserContext.Provider>
  );
};

export const useUserinfo = () => {
  const {userinfo} = useContext(UserContext);

  return userinfo;
};
export const useSetUser = () => {
  const {setUser} = useContext(UserContext);

  return setUser;
};
