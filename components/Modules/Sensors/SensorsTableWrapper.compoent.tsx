import classNames from 'classnames';
import React, {FunctionComponent, useCallback} from 'react';
import {Col, Row} from 'reactstrap';
import withSearch from '../../Common/hooks/useSearch.hook';

import styles from '../../../styles/components/Sensors/Table.module.scss';
import useSensorsFilter from './Hooks/useSensorsFilter.hook';
import usePagination from '../../Common/hooks/usePagination.hook';
import {sensorsPerPage} from '../../Helpers/Variables';

import dynamic from 'next/dynamic';
import {useSensorsTable} from './Hooks/useSensorsTable';

const SensorTable = dynamic(() => import('./SensorsTable.component'), {
  ssr: false,
});

const SensorsTableWrapper: FunctionComponent = () => {
  const SearchForm = withSearch();
  const {PaginationNav, clearPageState} = usePagination(sensorsPerPage);
  const {FilterPanel} = useSensorsFilter();

  const {sensorsData, sensorsIsLoading, onSearch, onPageChanged, onChangeType} =
    useSensorsTable();

  const filterAction = useCallback(
    (action) => {
      onChangeType(action);
      clearPageState();
    },
    [clearPageState, onChangeType],
  );

  return (
    <>
      <Row className="mb-3">
        <Col md="6" lg="8">
          <FilterPanel filterAction={filterAction} />
        </Col>
        <Col md="6" lg="4">
          <SearchForm
            loading={sensorsIsLoading}
            onSearch={onSearch}
            className={classNames(
              'd-flex align-items-center w-100 mt-3 mb-3',
              styles.SensorsSearchForm,
            )}
          />
        </Col>
      </Row>
      <SensorTable />
      <PaginationNav total={sensorsData.count} onPageChange={onPageChanged} />
    </>
  );
};
export default SensorsTableWrapper;
