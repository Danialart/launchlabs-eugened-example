import { FunctionComponent } from 'react'
import { FormFeedback, FormGroup, Input, Label } from 'reactstrap'
import { InputProps, InputType } from 'reactstrap/types/lib/Input'
import FilePreviewImage from '../../Common/FilePreviewImage.component'
import useFileInputImage from '../../Common/hooks/useFileInputImage.hook'
import { Sensor } from '../../Types/Sensor.types'
import { useSensorForm } from './Hooks/useSensorForm.hook'
import styles from '../../../styles/components/Sensors/SensorForm.module.scss'

const SensorFormGroup: FunctionComponent<
  {
    label: string
    fieldName: keyof Omit<Sensor, 'id' | 'created' | 'updated' | 'type'>
    type?: InputType
    placeholder?: string
  } & InputProps
> = ({ label, fieldName, placeholder, type, ...args }) => {
  const { sensor, validationErrors, onChange } = useSensorForm()
  const { onFileChanged, filePreview } = useFileInputImage()

  return (
    <FormGroup className={styles.formInput}>
      <Label for="title">{label}:</Label>
      {fieldName === 'photo' && (
        <FilePreviewImage
          photo={
            filePreview ||
            (sensor.photo && `${sensor.photo as string}?width=500`)
          }
        />
      )}
      <Input
        type={type ?? 'text'}
        name="title"
        className={styles.input}
        invalid={!!validationErrors[fieldName]}
        value={
          type !== 'file'
            ? (sensor[fieldName] as string | number | string[]) ?? ''
            : undefined
        }
        checked={!!(type === 'checkbox' && sensor[fieldName]) ?? undefined}
        placeholder={placeholder}
        onChange={(e) => {
          if (type === 'file' && e.target.files?.length) {
            onFileChanged(e.target.files[0])
            onChange(fieldName, e.target.files[0])
          } else
            onChange(
              fieldName,
              type === 'checkbox' ? e.target.checked : e.target.value,
            )
        }}
        {...args}
      />
      <FormFeedback>{validationErrors.title}</FormFeedback>
    </FormGroup>
  )
}

export default SensorFormGroup
