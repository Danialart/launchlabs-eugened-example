import { FunctionComponent, useMemo } from "react";
import DataTable from "react-data-table-component";
import { getSensorTypeName } from "../../Helpers/Functions";
import moment from "moment-timezone";
import { useSensorsTable } from "./Hooks/useSensorsTable";
import useSensorDelete from "./Hooks/useSensorDelete.hook";
import useSensorToggler from "./Hooks/useSensorToggler.hook";
import TableActionButtons from "../../Common/TableActionButtons.component";

const SensorTable: FunctionComponent = () => {
  const { sensorsIsLoading, sensorsData } = useSensorsTable();

  const noData = useMemo(
    () =>
      sensorsIsLoading ? null : (
        <div className="my-4">Нет результатов для показа</div>
      ),
    [sensorsIsLoading]
  );

  const handleSensorDelete = useSensorDelete();
  const toggleSensorStatus = useSensorToggler();

  const { onSensorStatusChanged, onDeletedSensor } = useSensorsTable();

  return (
    <div className="mb-4">
      <DataTable
        responsive={true}
        noDataComponent={noData}
        columns={[
          {
            name: "#",
            selector: (row) => row.number,
            sortable: true,
          },
          {
            name: "Название датчика",
            sortable: true,
            selector: (row) => row.title,
            format: (row) => (row.title.length ? row.title : "Без названия"),
          },
          {
            name: "Тип датчика",
            sortable: true,
            selector: (row) => row.type,
            format: (row) => <strong>{getSensorTypeName(row.type)}</strong>,
          },
          {
            name: "Координаты",
            sortable: false,
            cell: (row) => (
              <strong>
                {row.lat}, {row.lng}
              </strong>
            ),
          },
          {
            name: "Обновлено",
            sortable: true,
            selector: (row) =>
              moment
                .unix(row.updated)
                .tz("Europe/Moscow")
                .format("HH:mm MM.DD.YYYY"),
          },
          {
            name: "Статус датчика",
            sortable: true,
            selector: (row) => row.enabled,
            format: (row) =>
              row.enabled ? (
                <strong className="text-success">Включён</strong>
              ) : (
                <strong className="text-danger">Отключен</strong>
              ),
          },
          {
            name: "Действия",
            wrap: true,
            center: true,
            compact: true,
            width: "160px",
            cell: (sensor) => (
              <TableActionButtons
                editLink={`/sensors/edit/${sensor.id}`}
                viewLink={`/sensors/view/${sensor.id}`}
                isEnabled={sensor.enabled}
                onChangeStatus={() =>
                  toggleSensorStatus(sensor.id, onSensorStatusChanged)
                }
                onDelete={() =>
                  handleSensorDelete(sensor.id, sensor.number, onDeletedSensor)
                }
              />
            ),
          },
        ]}
        data={sensorsData.sensors}
      />
    </div>
  );
};

export default SensorTable;
