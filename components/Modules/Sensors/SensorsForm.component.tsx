import { AxiosError } from "axios";
import router from "next/router";
import React, { FunctionComponent, useCallback, useMemo } from "react";
import { Button, Col, Form, FormGroup, Row } from "reactstrap";
import useHandlingMessages from "../../Common/hooks/useHandlingMessages.hook";

import { useSensorForm } from "./Hooks/useSensorForm.hook";
import SensorFormGroup from "./SensorFormGroup.component";

const SensorForm: FunctionComponent<{
  type: "edit" | "create";
}> = ({ type }) => {
  const { handleServicesError, handleSuccessMessage } = useHandlingMessages();
  const { handleSubmit } = useSensorForm();

  const handleFormSubmit = useCallback(
    async (e) => {
      e.preventDefault();
      try {
        await handleSubmit(type);
        router.push("/");
        handleSuccessMessage("Изменения по датчику успешно сохранены!");
      } catch (error) {
        console.error(error);
        handleServicesError(
          error as AxiosError,
          error
            ? "Ошибка валидации полей, заполните все поля!"
            : "Неизвестная ошибка, попробуйте обновить страницу и попробовать ещё раз!"
        );
      }
    },
    [handleServicesError, handleSubmit, type, handleSuccessMessage]
  );

  const formStyle = useMemo(() => ({ maxWidth: 992 }), []);

  const formChildren = useMemo(
    () => (
      <>
        <SensorFormGroup
          placeholder="Введите название датчика"
          fieldName="title"
          label="Название датчика"
        />
        <Row>
          <Col>
            <SensorFormGroup
              placeholder="00.00000"
              fieldName="lat"
              label="Координаты (широта)"
            />
          </Col>
          <Col>
            <SensorFormGroup
              placeholder="00.00000"
              fieldName="lng"
              label="Координаты (долгота)"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <SensorFormGroup
              placeholder="Введите локацию"
              label="Локация"
              fieldName="location"
            />
          </Col>
          <Col>
            <SensorFormGroup
              placeholder="Введите номер датчика"
              label="Номер датчика"
              fieldName="number"
              type="number"
            />
          </Col>
        </Row>

        <Row>
          <Col>
            <SensorFormGroup
              type="checkbox"
              fieldName="ignore_coords"
              label="Игнорировать координаты"
            />
          </Col>
          <Col>
            <SensorFormGroup
              type="checkbox"
              fieldName="enabled"
              label="Датчик включён"
            />
          </Col>
        </Row>
        <FormGroup>
          <Button color="success">
            {type === "create" ? "Добавить" : "Сохранить"} датчик
          </Button>
        </FormGroup>
      </>
    ),
    [type]
  );

  return (
    <Form
      className="my-4 mx-auto"
      style={formStyle}
      onSubmit={handleFormSubmit}
    >
      {formChildren}
    </Form>
  );
};
export default React.memo(SensorForm);
