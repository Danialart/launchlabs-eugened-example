import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import useHandlingMessages from '../../../Common/hooks/useHandlingMessages.hook';
import {sensorsPerPage} from '../../../Helpers/Variables';
import SensorsService from '../../../services/sensors.service';
import {FilterSensors, Sensor} from '../../../Types/Sensor.types';
import {SensorFilterType} from './useSensorsFilter.hook';

export type ReturnType = {
  sensorsData: {
    sensors: Sensor[];
    count: number;
  };
  onChangeType: (action: SensorFilterType) => void;
  onSearch: (searchQuery: any) => void;
  onPageChanged: (offset: number) => void;
  onDeletedSensor: (id: any) => void;
  onSensorStatusChanged: (id: number) => void;
  sensorsIsLoading: boolean;
};

const sensorsTableContext = React.createContext<ReturnType>({} as ReturnType);

function useSensorsTableContextData(initialData?: Sensor[]): ReturnType {
  const {handleServicesError} = useHandlingMessages();

  const [sensorsIsLoading, setSensorsIsLoading] = useState<boolean>(false);

  // Mount controller
  const isMounted = useRef<boolean>(false);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const [sensorsData, setSensorsData] = useState<ReturnType['sensorsData']>({
    count: 0,
    sensors: initialData ?? ([] as Sensor[]),
  });

  const [sensorsFilter, setSensorsFilter] = useState<FilterSensors>({
    offset: 0,
    limit: sensorsPerPage,
  });

  // Request sensors every time when some of filter data is changed or if its first time render and filters are empty
  useEffect(() => {
    const sensorsService = new SensorsService();

    setSensorsIsLoading(true);
    sensorsService
      .getSensors(sensorsFilter)
      .then(([sensors, count]) => {
        if (isMounted.current)
          setSensorsData({
            sensors,
            count,
          });
      })
      .catch((error) =>
        handleServicesError(error, 'Ошибка получения списка датчиков'),
      )
      .finally(() => setSensorsIsLoading(false));
  }, [sensorsFilter, handleServicesError]);

  const changeSensorFilterParams = useCallback(
    (data: FilterSensors) => {
      if (!sensorsIsLoading) {
        const actuallyChanged: FilterSensors = {};

        // Check if new values is actually different than stored in the state
        Object.entries(data).map(([key, value]) => {
          if (sensorsFilter[key as keyof FilterSensors] != value)
            actuallyChanged[key as keyof FilterSensors] = value;
        });

        if (Object.entries(actuallyChanged).length)
          setSensorsFilter({
            ...sensorsFilter,
            ...actuallyChanged,
          });
      }
    },
    [sensorsFilter, sensorsIsLoading],
  );

  const onChangeType = useCallback(
    (action: SensorFilterType) => {
      // Set new type and clear previous offset filters
      changeSensorFilterParams({
        type: action === 'clear' ? undefined : action,
        offset: 0,
        limit: sensorsPerPage,
      });
    },
    [changeSensorFilterParams],
  );

  const onSearch = useCallback(
    (searchQuery) => {
      changeSensorFilterParams({
        [parseInt(searchQuery) ? 'number' : 'title']: searchQuery,
      });
    },
    [changeSensorFilterParams],
  );

  const onPageChanged = useCallback(
    (offset: number) => {
      changeSensorFilterParams({offset});
    },
    [changeSensorFilterParams],
  );

  const onDeletedSensor = useCallback(
    (id) => {
      // Get results and actual length of sensors list
      setSensorsData({
        sensors: sensorsData.sensors.filter((sensor) => sensor.id !== id),
        count: sensorsData.count - 1,
      });
    },
    [sensorsData],
  );

  const onSensorStatusChanged = useCallback(
    (id: number) => {
      const sensorIndex = sensorsData.sensors.findIndex(
        (sensor) => sensor.id === id,
      );

      if (~sensorIndex) {
        const sensors = [...sensorsData.sensors];
        const sensor = sensors[sensorIndex];

        sensors[sensorIndex] = {
          ...sensor,
          enabled: !sensor.enabled,
        };
        setSensorsData({
          ...sensorsData,
          sensors,
        });
      }
    },
    [sensorsData],
  );

  return {
    sensorsData,
    onChangeType,
    onSearch,
    onPageChanged,
    onDeletedSensor,
    onSensorStatusChanged,
    sensorsIsLoading,
  };
}

export const SensorTableContextWrapper: FunctionComponent<{
  initialData?: Sensor[];
}> = ({initialData, children}) => {
  return (
    <sensorsTableContext.Provider
      value={useSensorsTableContextData(initialData)}
    >
      {children}
    </sensorsTableContext.Provider>
  );
};

export const useSensorsTable = () => {
  const values = useContext(sensorsTableContext);
  return values;
};
