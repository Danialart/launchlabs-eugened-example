import { FunctionComponent, useState } from 'react'
import { Button } from 'reactstrap'
import { FilterSensors, SensorType } from '../../../Types/Sensor.types'

export type SensorFilterType = FilterSensors['type'] | 'clear'
type ReturnType = {
  FilterPanel: FunctionComponent<{
    filterAction: (action: SensorFilterType) => void
  }>
}
type ButtonType = { action: SensorFilterType; title: string }

export default function useSensorsFilter(): ReturnType {
  const [filter, setFilter] = useState<SensorFilterType | undefined>()

  const FilterButton = ({
    action,
    title,
    filterAction,
  }: ButtonType & { filterAction: (action: SensorFilterType) => void }) => {
    const handleClick = (action: SensorFilterType) => {
      setFilter(action)
      filterAction(action)
    }
    return (
      <Button
        color="warning"
        className="mx-lg-2 my-1 mx-1"
        active={filter === action}
        onClick={() => handleClick(action)}
      >
        {title}
      </Button>
    )
  }

  const buttonsList: ButtonType[] = [
    {
      action: 'clear',
      title: 'Все',
    },
    {
      action: SensorType.BOUY,
      title: 'Буй',
    },
    {
      action: SensorType.SHIP,
      title: 'Корабль',
    },
    {
      action: SensorType.SPUTNIK,
      title: 'Спутник',
    },
    {
      action: SensorType.CHECKPOINT,
      title: 'Чекпоинт',
    },
  ]

  const FilterPanel: FunctionComponent<{
    filterAction: (action: SensorFilterType) => void
  }> = ({ filterAction }) => {
    return (
      <div className="d-flex align-items-center justify-content-sm-between justify-content-md-start flex-wrap">
        {buttonsList.map((button) => (
          <FilterButton
            filterAction={filterAction}
            key={button.action}
            action={button.action}
            title={button.title}
          />
        ))}
      </div>
    )
  }

  return { FilterPanel }
}
