import useHandlingMessages from "../../../Common/hooks/useHandlingMessages.hook";
import SensorsService from "../../../services/sensors.service";
import { Sensor } from "../../../Types/Sensor.types";

type HandleDeleteFunction = (
  id: Sensor["id"],
  number: Sensor["number"],
  cb?: (id: Sensor["id"]) => void
) => void;

export default function useSensorDelete(): HandleDeleteFunction {
  const { handleServicesError } = useHandlingMessages();

  const handleSensorDelete: HandleDeleteFunction = (
    id: Sensor["id"],
    number: Sensor["id"],
    cb?: (id: Sensor["id"]) => void
  ) => {
    if (confirm(`Вы действительно хотите удалить датчик ${number}`)) {
      const sensorService = new SensorsService();

      sensorService
        .deleteSensor(id)
        .then(() => cb && cb(id))
        .catch((err) => {
          handleServicesError(err, "Неизвестная ошибка удаления датчика!");
        });
    }
  };

  return handleSensorDelete;
}
