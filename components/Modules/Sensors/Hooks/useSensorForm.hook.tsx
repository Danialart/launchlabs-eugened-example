import { isLatitude, isLongitude, isString, length } from "class-validator";
import React, {
  FunctionComponent,
  Reducer,
  useCallback,
  useContext,
  useEffect,
  useReducer,
} from "react";
import SensorsService from "../../../services/sensors.service";
import { ValueOf } from "../../../Types/CommonTypes";
import { Sensor } from "../../../Types/Sensor.types";

/**
 * Types for this component
 */
type ReturningType = {
  onChange: (fieldName: keyof Sensor, value: ValueOf<Sensor>) => void;
  validationErrors: ValidationSensorType;
  sensor: Sensor;
  handleSubmit: (type: "create" | "edit") => Promise<Sensor>;
};

type ValidationSensorType = {
  [x in keyof Omit<Sensor, "id" | "created" | "updated" | "type">]?: string;
};

type SensorReducerActionType = {
  type: "onChange";
  fieldName: keyof Sensor;
  fieldValue: ValueOf<Sensor>;
};

type ValidationErrorsActionType = {
  type: "hasError" | "noError";
  fieldName: SensorReducerActionType["fieldName"];
  fieldValue?: string;
};

//

const FormContext = React.createContext<ReturningType>({} as ReturningType);

const SensorFormReducer: Reducer<Sensor, SensorReducerActionType> = (
  state,
  action
): Sensor => {
  const { type, fieldName, fieldValue } = action;
  switch (type) {
    case "onChange":
      return { ...state, [fieldName]: fieldValue };

    default:
      return state;
  }
};

const ValidationErrorsReducer: Reducer<
  ValidationSensorType,
  ValidationErrorsActionType
> = (state, action) => {
  const { type, fieldName, fieldValue } = action;

  switch (type) {
    case "hasError":
      return { ...state, [fieldName]: fieldValue };

    case "noError":
      return { ...state, [fieldName]: undefined };
    default:
      return state;
  }
};

function CreateFormContextData(initialData?: Sensor): ReturningType {
  // Remove notice from received data
  if (initialData) delete initialData.lastNotice;

  const [sensor, dispatchSensor] = useReducer<
    Reducer<Sensor, SensorReducerActionType>
  >(
    SensorFormReducer,
    initialData ??
      ({
        ignore_coords: false,
        enabled: false,
      } as Sensor)
  );

  // Validation error state object
  const [validationErrors, dispatchValidationErrors] = useReducer<
    Reducer<ValidationSensorType, ValidationErrorsActionType>
  >(ValidationErrorsReducer, {});

  const onChange = useCallback(
    (fieldName: keyof Sensor, value: ValueOf<Sensor>) =>
      dispatchSensor({
        type: "onChange",
        fieldName,
        fieldValue: value,
      }),
    []
  );

  const handleSubmit = useCallback(
    (type: "edit" | "create") => {
      return new Promise<Sensor>((resolve, reject) => {
        if (
          Object.entries(validationErrors).filter(([key, value]) => {
            value !== undefined;
          }).length
        )
          return reject("validation error");

        const sensorService = new SensorsService();

        let result: Promise<Sensor>;

        const { created, updated, ...sensorInfo } = sensor;

        if (type === "create")
          result = sensorService.createSensor(sensorInfo as Sensor);
        else result = sensorService.updateSensor(sensorInfo as Sensor);

        result.then((res) => resolve(res)).catch((err) => reject(err));
      });
    },
    [sensor, validationErrors]
  );

  const validateFields = useCallback((sensor: Sensor) => {
    const foundErrors: ValidationSensorType = {};

    (Object.keys(sensor) as Array<keyof ValidationSensorType>).map((k) => {
      const value = sensor[k];

      if (k === "title") {
        if (!isString(value) || !length(value, 5))
          dispatchValidationErrors({
            type: "hasError",
            fieldName: "title",
            fieldValue:
              "Ошибка названия датчика. Поле не должно быть пустым и состоять минимум из 5 символов.",
          });
        else
          dispatchValidationErrors({
            type: "noError",
            fieldName: "title",
          });
      }
      if (k === "lat")
        if (!isLatitude(value as string))
          dispatchValidationErrors({
            type: "hasError",
            fieldName: "lat",
            fieldValue: "Некорректные координаты",
          });
        else
          dispatchValidationErrors({
            type: "noError",
            fieldName: "lat",
          });
      if (k === "lng")
        if (!isLongitude(value as string))
          dispatchValidationErrors({
            type: "hasError",
            fieldName: "lng",
            fieldValue: "Некорректные координаты",
          });
        else
          dispatchValidationErrors({
            type: "noError",
            fieldName: "lng",
          });
    });

    return foundErrors;
  }, []);

  // Validate fields
  useEffect(() => {
    validateFields(sensor);
  }, [sensor, validateFields]);

  return { sensor, onChange, validationErrors, handleSubmit };
}

export const SensorFormContext: FunctionComponent<{ initialData?: Sensor }> = ({
  initialData,
  children,
}) => {
  return (
    <FormContext.Provider value={CreateFormContextData(initialData)}>
      {children}
    </FormContext.Provider>
  );
};

// Hook to provide context values and form handler function
export const useSensorForm = (): ReturningType => {
  const { onChange, validationErrors, handleSubmit, sensor } =
    useContext(FormContext);

  return { sensor, onChange, validationErrors, handleSubmit };
};
