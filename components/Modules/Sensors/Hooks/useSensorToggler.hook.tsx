import useHandlingMessages from "../../../Common/hooks/useHandlingMessages.hook";
import SensorsService from "../../../services/sensors.service";
import { Sensor } from "../../../Types/Sensor.types";

const useSensorToggler = () => {
  const { handleServicesError, handleSuccessMessage } = useHandlingMessages();
  const toggleSensorStatus = (
    id: Sensor["id"],
    callback?: (id: Sensor["id"]) => void
  ) => {
    const sensorService = new SensorsService();

    sensorService
      .toggleSensor(id)
      .then(() => {
        handleSuccessMessage("Сенсор был успешно изменён!");
        if (callback) callback(id);
      })
      .catch((error) => handleServicesError(error));
  };

  return toggleSensorStatus;
};

export default useSensorToggler;
