import { FunctionComponent } from "react";
import usePagination from "../../Common/hooks/usePagination.hook";
import { noticesPerPage } from "../../Helpers/Variables";
import { useNoticeTable } from "../Notices/Hooks/useNoticesTable.hook";
import NoticesTable from "../Notices/NoticesTable.component";

const SensorNoticesList: FunctionComponent = () => {
  const { PaginationNav } = usePagination(noticesPerPage);

  const { noticesData, onPageChanged } = useNoticeTable();

  return (
    <div>
      <h2>Полученные данные ({noticesData.count ?? 0})</h2>
      <NoticesTable />
      <PaginationNav onPageChange={onPageChanged} total={noticesData.count} />
    </div>
  );
};
export default SensorNoticesList;
