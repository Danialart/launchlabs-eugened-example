import classNames from "classnames";
import { FunctionComponent } from "react";
import { Col, Row } from "reactstrap";
import usePagination from "../../Common/hooks/usePagination.hook";
import withSearch from "../../Common/hooks/useSearch.hook";
import { sensorsPerPage } from "../../Helpers/Variables";
import { useUsersTable } from "./hooks/useUsersTable.hook";
import styles from "../../../styles/components/Sensors/Table.module.scss";
import UsersTable from "./UsersTable.component";

const UsersTableWrapper: FunctionComponent = () => {
  const SearchForm = withSearch();
  const { PaginationNav } = usePagination(sensorsPerPage);

  const { usersData, usersIsLoading, onSearch, onPageChanged } =
    useUsersTable();

  return (
    <>
      <Row className="mb-3">
        <Col md="6" lg="5">
          <SearchForm
            loading={usersIsLoading}
            onSearch={onSearch}
            className={classNames(
              "d-flex align-items-center w-100 mt-3 mb-3",
              styles.SensorsSearchForm
            )}
          />
        </Col>
      </Row>
      <UsersTable />
      <PaginationNav total={usersData.count} onPageChange={onPageChanged} />
    </>
  );
};

export default UsersTableWrapper;
