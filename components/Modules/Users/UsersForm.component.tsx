import { FunctionComponent } from "react";
import { Button, Col, Form, FormGroup, Row } from "reactstrap";
import { HumanRoleName } from "../../Helpers/Variables";
import { User } from "../../Types/UserTypes";
import useUserForm from "./hooks/useUsersForm.hook";
import UserFormInput from "./UserFormInput.component";

const UsersForm: FunctionComponent<{
  type: "edit" | "create";
  user?: User;
}> = ({ user, type }) => {
  const {
    onChangeUserField,
    userData,

    getFieldInvalidStatus,
    getValidationErrorsTextForInput,
    handleSubmit,
  } = useUserForm(type, user);

  return (
    <Form
      className="my-3 mx-auto"
      style={{ maxWidth: "768px" }}
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit();
      }}
    >
      <UserFormInput
        name="name"
        placeholder="Ivan"
        autoComplete="new-password"
        value={userData.name}
        onChange={onChangeUserField}
        isInvalid={getFieldInvalidStatus}
        getValidationError={getValidationErrorsTextForInput}
        label="Логин пользователя"
      />
      <UserFormInput
        name="email"
        placeholder="mail@mail.net"
        type="email"
        value={userData.email}
        onChange={onChangeUserField}
        isInvalid={getFieldInvalidStatus}
        getValidationError={getValidationErrorsTextForInput}
        label="E-mail пользователя"
      />
      <UserFormInput
        name="fio"
        placeholder="Михаилов Иван Васильевич"
        value={userData.fio}
        onChange={onChangeUserField}
        isInvalid={getFieldInvalidStatus}
        getValidationError={getValidationErrorsTextForInput}
        label="ФИО пользователя (не обязательно)"
      />

      <Row>
        <Col md="6">
          <UserFormInput
            name="password"
            type="password"
            autoComplete="new-password"
            placeholder="********"
            value={userData.password}
            onChange={onChangeUserField}
            isInvalid={getFieldInvalidStatus}
            getValidationError={getValidationErrorsTextForInput}
            label="Пароль"
          />
        </Col>
        <Col md="6">
          <UserFormInput
            name="passwordConfirm"
            type="password"
            placeholder="********"
            value={userData.passwordConfirm}
            onChange={onChangeUserField}
            isInvalid={getFieldInvalidStatus}
            getValidationError={getValidationErrorsTextForInput}
            label="Повтор пароля"
          />
        </Col>
      </Row>
      <UserFormInput
        name="role"
        type="select"
        multiple
        onChange={onChangeUserField}
        isInvalid={getFieldInvalidStatus}
        getValidationError={getValidationErrorsTextForInput}
        label="Права пользователя"
        value={userData.role?.map((role) => role.role_name) ?? []}
      >
        {Object.entries(HumanRoleName).map(([roleKey, roleName]) => (
          <option key={roleKey} value={roleKey}>
            {roleName}
          </option>
        ))}
      </UserFormInput>

      <hr />
      <FormGroup className="d-flex justify-content-end">
        <Button color="success">
          {type === "create" ? "Добавить пользователя" : "Сохранить данные"}
        </Button>
      </FormGroup>
    </Form>
  );
};

export default UsersForm;
