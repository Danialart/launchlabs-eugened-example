import { FunctionComponent } from "react";
import { FormGroup, Label, Input } from "reactstrap";
import { InputType, InputProps } from "reactstrap/types/lib/Input";
import { ValueOf } from "../../Types/CommonTypes";
import { Role, User } from "../../Types/UserTypes";
import { UserWithPassword } from "./hooks/useUsersForm.hook";

type Props = {
  type?: InputType;
  value?: InputProps["value"];
  name: keyof UserWithPassword;
  placeholder?: string;
  onChange: (
    fieldName: keyof UserWithPassword,
    fieldValue: ValueOf<User>
  ) => void;
  isInvalid: (fieldName: keyof UserWithPassword) => boolean;
  getValidationError: (fieldName: keyof UserWithPassword) => JSX.Element[];
  label: string;
} & Omit<InputProps, "onChange">;
const UserFormInput: FunctionComponent<Props> = ({
  type,
  value,
  label,
  name,
  placeholder,
  getValidationError,
  isInvalid,
  onChange,
  children,
  ...rest
}) => {
  return (
    <FormGroup>
      <Label for={name}>{label}:</Label>
      <Input
        name={name}
        id={name}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={(e: any) => {
          if (type === "select") {
            const { selectedOptions } = e.target;

            onChange(
              name,
              Array.from(
                selectedOptions,
                (option: HTMLOptionElement) => option.value
              ).map((role) => ({ role_name: role as Role }))
            );
          } else onChange(name, e.target.value);
        }}
        invalid={isInvalid(name)}
        {...rest}
      >
        {children}
      </Input>
      {getValidationError(name)}
    </FormGroup>
  );
};

export default UserFormInput;
