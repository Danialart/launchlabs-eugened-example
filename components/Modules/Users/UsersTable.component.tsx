import { FunctionComponent } from "react";
import DataTable from "react-data-table-component";
import TableActionButtons from "../../Common/TableActionButtons.component";
import { HumanRoleName } from "../../Helpers/Variables";

import { useUsersTable } from "./hooks/useUsersTable.hook";

const UsersTable: FunctionComponent = () => {
  const { usersIsLoading, usersData, onDeleteUser } = useUsersTable();

  return (
    <div className="mb-4">
      <DataTable
        responsive={true}
        noDataComponent={
          !usersIsLoading && (
            <div className="my-4">Нет результатов для показа</div>
          )
        }
        columns={[
          {
            name: "#",
            selector: (row) => row.id,
            sortable: true,
          },
          {
            name: "Логин",
            cell: (row) => <strong>{row.name}</strong>,
            sortable: true,
          },
          {
            name: "ФИО",
            selector: (row) => row.fio,
          },
          {
            name: "E-mail",
            selector: (row) => row.email,
          },
          {
            name: "Права пользователя",
            sortable: true,
            cell: (row) => (
              <div className="d-flex flex-column">
                {row.role.map((role) => (
                  <div key={role.role_name} className="d-block">
                    <strong>{HumanRoleName[role.role_name]}</strong>
                  </div>
                ))}
              </div>
            ),
          },
          {
            name: "Действия",
            right: true,
            cell: (user) => (
              <TableActionButtons
                editLink={`/users/edit/${user.id}`}
                onDelete={() => onDeleteUser(user.id)}
              />
            ),
          },
        ]}
        data={usersData.users}
      />
    </div>
  );
};

export default UsersTable;
