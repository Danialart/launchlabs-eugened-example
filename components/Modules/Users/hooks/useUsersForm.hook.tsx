import { useCallback, useEffect, useMemo, useState } from "react";
import { ValueOf } from "../../../Types/CommonTypes";
import { Role, User } from "../../../Types/UserTypes";
import * as yup from "yup";
import { FormFeedback } from "reactstrap";
import UserService from "../../../services/users.service";
import useHandlingMessages from "../../../Common/hooks/useHandlingMessages.hook";
import { useRouter } from "next/router";

type validationErorsObjectType = {
  fieldError: string;
  error: string;
  rolesByDefault: Role[];
  getFieldInvalidStatus: (inputName: keyof UserWithPassword) => boolean;
  getValidationErrorsTextForInput: (
    inputName: keyof UserWithPassword
  ) => JSX.Element[];
  handleSubmit: () => void;
};

export type UserWithPassword = User & {
  password: string;
  passwordConfirm: string;
};

type ReturnType = {
  onChangeUserField: (
    fieldName: keyof UserWithPassword,
    fieldValue: ValueOf<User>
  ) => void | boolean;
  userData: UserWithPassword;
};

const formSchema = yup.object().shape({
  name: yup
    .string()
    .min(3, {
      fieldError: "name",
      error: "Длинна поля должна быть более 3 символов",
    })
    .required({
      fieldError: "name",
      error: "Поле обязательно для заполнения",
    }),
  fio: yup
    .string()
    .matches(/[а-яА-Я ]/, {
      message: {
        fieldError: "fio",
        error: "Поле должно содержать только кирилицу и пробелы",
      },
    })
    .min(3, {
      fieldError: "fio",
      error: "Поле должно быть минимум 3 смивола",
    }),
  email: yup
    .string()
    .email({
      fieldError: "email",
      error: "Введите корректный E-mail",
    })
    .required({
      fieldError: "email",
      error: "Поле обязательно для заполнения",
    }),
  password: yup
    .string()
    .min(8, {
      fieldError: "password",
      error: "Длинна пароля должна быть 8 или более символов",
    })
    .optional(),
  passwordConfirm: yup.string().test(
    "password-match",
    {
      fieldError: "passwordConfirm",
      error: "Пароли должны совпадать",
    },
    (value, context) => context.parent.password === value
  ),
  role: yup
    .array()
    .of(
      yup.object().shape({
        role_name: yup
          .string()
          .oneOf([Role.Client, Role.Admin, Role.SuperAdmin, Role.User], {
            error: "Выберите роль из списка",
            fieldError: "role",
          }),
      })
    )
    .required({
      error: "Выберите роль из списка",
      fieldError: "role",
    }),
});

export default function useUserForm(
  type: "create" | "edit",
  initialData?: User
) {
  // Format nullable values in object
  const initialUser = (
    initialData
      ? Object.fromEntries(
          Object.entries(initialData).map(([key, value]) => [key, value ?? ""])
        )
      : {}
  ) as UserWithPassword;

  const [user, setUser] = useState<ReturnType["userData"]>(initialUser);

  const [validationErrors, setValidationErrors] = useState<
    validationErorsObjectType[]
  >([]);

  const updateValidationErrorsObject = useCallback(
    (data: validationErorsObjectType[]) => {
      setValidationErrors(data);
    },
    []
  );

  const router = useRouter();

  const { handleServicesError, handleSuccessMessage } = useHandlingMessages();

  const validateFields = useCallback(
    (user: User) => {
      formSchema
        .validate(user, { abortEarly: false })
        .then(() =>
          // update old errors with a new empty array2
          updateValidationErrorsObject([])
        )
        .catch(function (err) {
          updateValidationErrorsObject(err.errors);
        });
    },
    [updateValidationErrorsObject]
  );

  const onChangeUserField: ReturnType["onChangeUserField"] = useCallback(
    (fieldName, fieldValue) => {
      setUser({
        ...user,
        [fieldName]: fieldValue,
      } as UserWithPassword);
    },
    [user]
  );

  const rolesByDefault = useMemo(() => {
    return user.role?.map((role) => role.role_name);
  }, [user]);

  const getValidationErrorsTextForInput = useCallback(
    (inputName: keyof UserWithPassword) =>
      validationErrors
        .filter((error) => error.fieldError === inputName)
        .map((error, index) => (
          <FormFeedback key={index}>{error.error}</FormFeedback>
        )),
    [validationErrors]
  );

  const getFieldInvalidStatus = useCallback(
    (inputName: keyof UserWithPassword) => {
      return !!validationErrors.find((error) => error.fieldError === inputName);
    },
    [validationErrors]
  );

  const handleSubmit = useCallback(() => {
    formSchema.isValid(user).then((valid) => {
      if (valid) {
        const userService = new UserService();

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { passwordConfirm, id, ...userData } = user;

        if (type === "create") {
          userService
            .addUser(userData)
            .then(() => {
              handleSuccessMessage("Пользователь успешно создан!");
              router.push("/users");
            })
            .catch((err) => handleServicesError(err));
        } else if (type === "edit") {
          userService
            .updateUser(id, userData)
            .then(() =>
              handleSuccessMessage("Данные о пользователе успешно обновлены!")
            )
            .catch((err) => handleServicesError(err));
        }
      } else {
        validateFields(user);
        handleServicesError(
          undefined,
          "Заполните все поля и попробуйте ещё раз!"
        );
      }
    });
  }, [
    user,
    type,
    handleSuccessMessage,
    router,
    handleServicesError,
    validateFields,
  ]);

  useEffect(() => {
    // Validate object every time when it changes
    if (Object.entries(user).length) validateFields(user);
  }, [user, validateFields]);

  return {
    onChangeUserField,
    userData: user,
    rolesByDefault,
    getFieldInvalidStatus,
    getValidationErrorsTextForInput,
    handleSubmit,
  };
}
