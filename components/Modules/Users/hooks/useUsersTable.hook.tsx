import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import useHandlingMessages from "../../../Common/hooks/useHandlingMessages.hook";
import { usersPerPage } from "../../../Helpers/Variables";
import UserService from "../../../services/users.service";
import { FilterUsers, User } from "../../../Types/UserTypes";

export type ReturnType = {
  usersData: {
    users: User[];
    count: number;
  };
  onSearch: (searchQuery: any) => void;
  onPageChanged: (offset: number) => void;
  onDeleteUser: (id: any) => void;
  usersIsLoading: boolean;
};

const usersTableContext = React.createContext<ReturnType>({} as ReturnType);

const deleteUser = (id: number) => {
  const userService = new UserService();

  return userService.deleteUser(id);
};

function useUsersTableContextData(initialData?: User[]): ReturnType {
  const { handleServicesError, handleSuccessMessage } = useHandlingMessages();

  const [usersIsLoading, setUsersIsLoading] = useState<boolean>(false);

  // Mount controller
  const isMounted = useRef<boolean>(false);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const [usersData, setUsersData] = useState<ReturnType["usersData"]>({
    count: 0,
    users: initialData ?? ([] as User[]),
  });

  const [usersFilter, setUsersFilter] = useState<FilterUsers>({
    offset: 0,
    limit: usersPerPage,
  });

  // Request sensors every time when some of filter data is changed or if its first time render and filters are empty
  useEffect(() => {
    const userService = new UserService();

    setUsersIsLoading(true);
    userService
      .getUsers(usersFilter)
      .then(([users, count]) => {
        if (isMounted.current)
          setUsersData({
            users,
            count,
          });
      })
      .catch((error) =>
        handleServicesError(error, "Ошибка получения списка пользователей")
      )
      .finally(() => setUsersIsLoading(false));
  }, [usersFilter, handleServicesError]);

  const changeSensorFilterParams = useCallback(
    (data: FilterUsers) => {
      if (!usersIsLoading) {
        const actuallyChanged: FilterUsers = {};

        // Check if new values is actually different than stored in the state
        Object.entries(data).map(([key, value]) => {
          if (usersFilter[key as keyof FilterUsers] != value)
            actuallyChanged[key as keyof FilterUsers] = value;
        });

        if (Object.entries(actuallyChanged).length)
          setUsersFilter({
            ...usersFilter,
            ...actuallyChanged,
          });
      }
    },
    [usersFilter, usersIsLoading]
  );

  const onSearch = useCallback(
    (searchQuery) => {
      changeSensorFilterParams({
        search: searchQuery,
      });
    },
    [changeSensorFilterParams]
  );

  const onPageChanged = useCallback(
    (offset: number) => {
      changeSensorFilterParams({ offset });
    },
    [changeSensorFilterParams]
  );

  const onDeleteUser = useCallback(
    (id) => {
      // Get results and actual length of sensors list
      if (confirm(`Вы действительно хотите удалить пользователя с ID ${id} ?`))
        deleteUser(id)
          .then(() => {
            setUsersData({
              users: usersData.users.filter((user) => user.id !== id),
              count: usersData.count - 1,
            });
            handleSuccessMessage("Пользователь успешно удалён!");
          })
          .catch((err) => handleServicesError(err));
    },
    [usersData, handleServicesError, handleSuccessMessage]
  );

  return {
    usersData,
    onSearch,
    onPageChanged,
    onDeleteUser,
    usersIsLoading,
  };
}

export const UserTableContextWrapper: FunctionComponent<{
  initialData?: User[];
}> = ({ initialData, children }) => {
  return (
    <usersTableContext.Provider value={useUsersTableContextData(initialData)}>
      {children}
    </usersTableContext.Provider>
  );
};

export const useUsersTable = () => {
  const values = useContext(usersTableContext);
  return values;
};
