import router from "next/router";
import { FunctionComponent } from "react";
import { Button } from "reactstrap";

interface PropsType {
  onChangeStatus: () => void;
  editLink: string;
  viewLink: string;
  onDelete: () => void;
  isEnabled: boolean;
}

const TableActionButtons: FunctionComponent<Partial<PropsType>> = ({
  onChangeStatus,
  isEnabled,
  onDelete,
  editLink,
  viewLink,
}) => {
  return (
    <div className="justify-content-center align-items-center">
      {onChangeStatus && (
        <Button
          color={isEnabled ? "success" : "dark"}
          className="mx-1"
          size="sm"
          onClick={() => onChangeStatus()}
        >
          {isEnabled ? (
            <i className="bi bi-toggle-on" />
          ) : (
            <i className="bi bi-toggle-off" />
          )}
        </Button>
      )}
      {editLink && (
        <Button
          color="success"
          className="mx-1"
          onClick={() => router.push(editLink)}
          size="sm"
        >
          <i className="bi bi-pen-fill"></i>
        </Button>
      )}
      {viewLink && (
        <Button
          color="primary"
          className="mx-1"
          onClick={() => router.push(viewLink)}
          size="sm"
        >
          <i className="bi bi-eye-fill"></i>
        </Button>
      )}
      {onDelete && (
        <Button
          color="danger"
          className="mx-1"
          onClick={() => {
            onDelete();
          }}
          size="sm"
        >
          <i className="bi bi-archive-fill"></i>
        </Button>
      )}
    </div>
  );
};

export default TableActionButtons;
