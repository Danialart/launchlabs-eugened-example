import React, { FunctionComponent } from 'react'
import { Button, Form, Input } from 'reactstrap'

// For testing pipeline
const useSearch: FunctionComponent<{
  onSearch: (searchQuery: string) => unknown
  loading?: boolean
  searchPlaceholder?: string
  className?: string
}> = ({ onSearch, searchPlaceholder, className, loading }) => {
  const searchRef = React.createRef<HTMLInputElement>()

  let timeout: NodeJS.Timeout

  const search = (query: string) => {
    if (timeout) clearTimeout(timeout)

    timeout = setTimeout(() => {
      onSearch(query)
    }, 600)
  }

  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault()
        onSearch(searchRef.current?.value ?? '')
      }}
      className={className}
    >
      <Input
        name="search"
        innerRef={searchRef}
        placeholder={searchPlaceholder ?? 'Введите фразу для поиска...'}
        onChange={(e) => search(e.target.value)}
      />

      <Button color="primary" disabled={loading}>
        Поиск
      </Button>
    </Form>
  )
}

export default function withSearch() {
  return useSearch
}
