import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap'

type ReturnType = {
  PaginationNav: FunctionComponent<{
    total: number
    onPageChange: (offset: number) => void
  }>
  clearPageState: () => void
}

export default function usePagination(perPage: number): ReturnType {
  const [activePage, setActivePage] = useState<number>(1)

  const clearPageState = () => setActivePage(1)

  const PaginationNav: FunctionComponent<{
    total: number
    onPageChange: (offset: number) => void
  }> = ({ total, onPageChange }) => {
    // Total pages count
    const pagesCount = useMemo(() => Math.ceil(total / perPage), [total])

    const goToPageWithCallback = useCallback(
      (pageNumber?: number) => {
        if (pageNumber) {
          setActivePage(pageNumber)
          const offset = pageNumber === 1 ? 0 : pageNumber * perPage - perPage
          onPageChange(offset)
        }
      },
      [onPageChange],
    )

    // When pages count is changed - check if we not on the bage, that not exist anymore.
    useEffect(() => {
      // Go to the last possible if our page is greater than total pages count
      if (activePage > pagesCount) goToPageWithCallback(pagesCount)
    }, [pagesCount, goToPageWithCallback])

    const PageItem: FunctionComponent<{
      title: string | number
      pageNumber?: number
    }> = useCallback(
      ({ title, pageNumber }): JSX.Element => (
        <PaginationItem
          active={activePage === pageNumber}
          disabled={!pageNumber}
        >
          <PaginationLink onClick={() => goToPageWithCallback(pageNumber)}>
            {title}
          </PaginationLink>
        </PaginationItem>
      ),
      [goToPageWithCallback],
    )

    const prevPageNumber = useMemo(
      () => (activePage - 1 > 0 ? activePage - 1 : undefined),
      [],
    )
    const nextPageNumber = useMemo(
      () => (activePage + 1 <= pagesCount ? activePage + 1 : undefined),
      [pagesCount],
    )

    const generatePagesItems = useCallback(() => {
      const maxPagesAtOnce =
        (process.env.NEXT_PUBLIC_MAX_PAGINATION_PAGES &&
          +process.env.NEXT_PUBLIC_MAX_PAGINATION_PAGES) ||
        10

      const step = Math.floor(activePage / maxPagesAtOnce)

      const remainderIsZero: boolean = activePage % maxPagesAtOnce === 0

      const startFrom =
        // If its first part of pagination - start from first page
        step === 0
          ? 1
          : //Check if the current page is divisible by a maxPagesAtOnce variabke, without a remainder and if current page is not the last page
          !remainderIsZero && activePage !== pagesCount
          ? // Show next part of pages then (for current step)
            step * maxPagesAtOnce + 1
          : // Otherwise - check if step is not first and start from previous closer number or from the first page
          step - 1 > 0
          ? (step - 1) * maxPagesAtOnce + 1
          : 1

      const endPage =
        // Check if pages that we need is not more than total pages available
        pagesCount - startFrom > maxPagesAtOnce
          ? // If we startinf from the first page - just increase this amount to number of maxPagesAtOnce variable
            startFrom === 1
            ? maxPagesAtOnce
            : // Otherwise calculate the nest part
              startFrom + maxPagesAtOnce - 1
          : pagesCount

      const items = []

      for (let page = startFrom; page <= endPage; page++) {
        items.push(<PageItem key={page} title={page} pageNumber={page} />)
      }

      return (
        <>
          {activePage > maxPagesAtOnce ? (
            <PageItem
              key={startFrom - 1}
              title="..."
              pageNumber={startFrom - 1}
            />
          ) : null}
          {items}
          {pagesCount > maxPagesAtOnce && endPage < pagesCount ? (
            <PageItem key={endPage + 1} title="..." pageNumber={endPage + 1} />
          ) : null}
        </>
      )
    }, [PageItem, pagesCount])

    return pagesCount > 1 ? (
      <Pagination className="d-flex justify-content-center my-3">
        <PageItem title="Назад" pageNumber={prevPageNumber} />
        {generatePagesItems()}
        <PageItem title="Вперед" pageNumber={nextPageNumber} />
      </Pagination>
    ) : null
  }

  return { PaginationNav, clearPageState }
}
