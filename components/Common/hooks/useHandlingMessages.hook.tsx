import { AxiosError } from "axios";
import { toast } from "react-toastify";
import parse from "html-react-parser";
import { useCallback } from "react";

export default function useHandlingMessages() {
  const handleServicesError = useCallback(
    (error?: AxiosError, errorMessage?: string) => {
      if (error) {
        const backendErrorMessage = error.response?.data.errorObject.message;

        const message = Array.isArray(backendErrorMessage)
          ? backendErrorMessage
              .map((text: string) => `<div>${text}</div>`)
              .join("")
          : error.response?.data.errorObject
          ? JSON.stringify(error.response?.data.errorObject)
          : error.response?.data;

        toast.error(message ? <>{parse(message)}</> : errorMessage);
      } else if (errorMessage) toast.error(errorMessage);
    },
    []
  );

  const handleSuccessMessage = useCallback((message: string) => {
    return toast.success(message);
  }, []);

  return { handleServicesError, handleSuccessMessage };
}
