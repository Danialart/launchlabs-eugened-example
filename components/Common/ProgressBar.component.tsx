import axios from 'axios';
import Router from 'next/router';
import NProgress from 'nprogress';

let timer: NodeJS.Timeout;
let state: string;
let activeRequests = 0;
const delay = 200;

NProgress.configure({
  showSpinner: true,
});

function load() {
  if (state === 'loading') {
    return;
  }
  state = 'loading';

  timer = setTimeout(function () {
    NProgress.start();
  }, delay); // only show progress bar if it takes longer than the delay
}

function stop() {
  if (activeRequests > 0) return;
  state = 'stop';

  clearTimeout(timer);
  NProgress.done();
}

axios.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    activeRequests = 1;
    load();
    return config;
  },
  function (error) {
    activeRequests = 0;
    stop();
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    activeRequests = 0;
    stop();
    return response;
  },
  function (error) {
    activeRequests = 0;
    stop();
    return Promise.reject(error);
  },
);

Router.events.on('routeChangeStart', load);
Router.events.on('routeChangeComplete', stop);
Router.events.on('routeChangeError', stop);

export default function ProgressBar() {
  return null;
}
