import {FunctionComponent, PropsWithChildren} from 'react';

const HeadingComponent: FunctionComponent<PropsWithChildren<{title: string}>> =
  ({title, children}) => {
    return (
      <>
        <div className="mb-3 d-flex justify-content-between align-items-center">
          <h1>{title}</h1>
          {children}
        </div>
        <hr />
      </>
    );
  };

export default HeadingComponent;
