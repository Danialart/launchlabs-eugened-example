import classNames from 'classnames';
import router, {useRouter} from 'next/router';
import {FunctionComponent, useCallback, useState} from 'react';
import {Nav, NavItem, NavLink} from 'reactstrap';

type LinksList = {
  title: string;
  link: string;
  [x: string]: any;
};
const Navigation: FunctionComponent<{
  linksList: LinksList[];
  className?: string;
  [x: string]: any;
}> = ({linksList, className, ...props}) => {
  const {isLinkIsCurrent} = useActiveLink();

  return (
    <Nav className={className} {...props}>
      {linksList.map(
        ({link, title, className, activeClass, ...linkProps}, index) => (
          <NavItem key={index}>
            <NavLink
              href="#"
              className={classNames(
                className,
                isLinkIsCurrent(link) ? activeClass : null,
              )}
              onClick={(e) => {
                e.preventDefault();
                router.push(link);
              }}
              {...linkProps}
            >
              {title}
            </NavLink>
          </NavItem>
        ),
      )}
    </Nav>
  );
};

export const useActiveLink = () => {
  const router = useRouter();
  const isLinkIsCurrent = useCallback(
    (link: string) => {
      if (router.pathname === '/') {
        if (link === '/') return true;
        else return false;
      } else if (link !== '/') return router.pathname.match(link);
    },
    [router.pathname],
  );

  return {isLinkIsCurrent};
};

export const useNavToggle = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const toggle = () => setIsOpen(!isOpen);

  return {toggle, isOpen};
};

export default Navigation;
